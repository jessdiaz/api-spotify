import {Response, Request} from 'express';
import { connect } from '../config/database';

export async function getPropietario(req: Request, res: Response): Promise<Response | void>{
    const conn = await connect();
    const dataTipoInmueble = await conn.query('SELECT * FROM Propietario');
    return res.json(dataTipoInmueble[0]);
}

export async function getIdPropietario(req: Request, res: Response): Promise<Response | void>{
    const {id} = req.params;

    const conn = await connect();
    const dataTipoInmueble = await conn.query('SELECT * FROM Propietario WHERE idPropietario = ' + id);
    return res.json(dataTipoInmueble[0]);
}

export async function postPropietario(req:Request, res:Response): Promise<Response | void>{
    let propietario = {
        idinmueble: req.body.idinmueble,
        documento: req.body.documento,
        nombres: req.body.nombres,
        apellidos: req.body.apellidos,
        telefono: req.body.telefono
    }

    const conn = await connect();
    await conn.query('INSERT INTO Propietario SET ?', propietario);
    return res.json({code: 200, info: 'propietario guardado!'});
}

export async function putPropietario(req:Request, res:Response): Promise<Response | void>{
    const conn = await connect();
    await conn.query("UPDATE Propietario SET idinmueble = " + req.body.idinmueble + ", documento = " + req.body.documento + 
    ", nombres = '" + req.body.nombres + "', apellidos = '" + req.body.apellidos + "', telefono = '" + req.body.telefono +
    "' WHERE IDPropietario = " + req.body.idpropietario);
    return res.json({code: 200, info: 'El propietario '+ req.body.idpropietario +' se ha actualizado!'});
}

export async function deletePropietario(req:Request, res:Response): Promise<Response | void>{
    const {id} = req.params;

    const conn = await connect();
    await conn.query('DELETE FROM Propietario WHERE idPropietario =' + id);
    return res.json({code: 200, info: 'Propietario '+ id +' Eliminado!'});
}