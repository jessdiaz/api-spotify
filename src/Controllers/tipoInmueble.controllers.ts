import {Response, Request} from 'express';
import { connect } from '../config/database';

export async function getTipos(req: Request, res: Response): Promise<Response | void>{
    const conn = await connect();
    const dataTipoInmueble = await conn.query('SELECT * FROM Tipo_Inmueble');
    return res.json(dataTipoInmueble[0]);
}

export async function getIdTipos(req: Request, res: Response): Promise<Response | void>{
    const {id} = req.params;

    const conn = await connect();
    const dataTipoInmueble = await conn.query('SELECT * FROM Tipo_Inmueble WHERE idtipo_inmueble = ' + id);
    return res.json(dataTipoInmueble[0]);
}

export async function postTipos(req:Request, res:Response): Promise<Response | void>{
    let tipo_inmueble = {
        idtipo_inmueble: req.body.idtipo_inmueble,
        descripcion: req.body.descripcion
    }

    const conn = await connect();
    await conn.query('INSERT INTO Tipo_Inmueble SET ?', tipo_inmueble);
    return res.json({code: 200, info: 'tipo de inmueble guardado!'});
}

export async function putTipos(req:Request, res:Response): Promise<Response | void>{
    const conn = await connect();
    await conn.query("UPDATE tipo_inmueble SET descripcion = '" + req.body.descripcion + "' WHERE idtipo_inmueble = " + req.body.idtipo_inmueble);
    return res.json({code: 200, info: 'El tipo de inmueble '+ req.body.idtipo_inmueble +' se ha actualizado!'});
}

export async function deleteTipos(req:Request, res:Response): Promise<Response | void>{
    const {id} = req.params;

    const conn = await connect();
    await conn.query('DELETE FROM Tipo_Inmueble WHERE idtipo_inmueble = ' + id);
    return res.json({code: 200, info: 'Inmueble '+ id +' Eliminado!'});
}