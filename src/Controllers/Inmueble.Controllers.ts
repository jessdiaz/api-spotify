import {Response, Request} from 'express';
import { connect } from '../config/database';

export async function getInmueble(req: Request, res: Response): Promise<Response | void>{
    const conn = await connect();
    const dataInmueble = await conn.query('SELECT * FROM Inmueble');
    return res.json(dataInmueble[0]);
}

export async function getIdInmueble(req: Request, res: Response): Promise<Response | void>{
    const {id} = req.params;
    
    const conn = await connect();
    const dataInmueble = await conn.query('SELECT * FROM Inmueble WHERE idinmueble = ' + id);
    return res.json(dataInmueble[0]);
}

export async function postInmueble(req:Request, res:Response): Promise<Response | void>{

    let Inmueble = {
        idinmueble: req.body.idinmueble,
        direccion: req.body.direccion,
        telefono: req.body.telefono,
        parqueadero: req.body.parqueadero,
        observaciones: req.body.observaciones,
        idtipo_inmueble: req.body.idtipo_inmueble
    }

    const conn = await connect();
    await conn.query('INSERT INTO Inmueble SET ?', Inmueble);
    return res.json({code: 200, info: 'Inmueble guardado!'});
}

export async function putInmueble(req:Request, res:Response): Promise<Response | void>{
    const conn = await connect();
    await conn.query("UPDATE inmueble SET direccion = '"+ req.body.direccion +"', telefono = '"+ req.body.telefono +"', parqueadero = '"+ req.body.parqueadero +"', observaciones = '"+ req.body.observaciones +"', idtipo_inmueble = '"+ req.body.idtipo_inmueble +"' where idinmueble = " + req.body.idinmueble);
    return res.json({code: 200, info: `El inmueble ${req.body.idinmueble} ha sido modificado con éxito`});
}

export async function deleteInmueble(req:Request, res:Response): Promise<Response | void>{
    const {id} = req.params;

    const conn = await connect();
    await conn.query('DELETE FROM Inmueble WHERE idInmueble ='+ id);
    return res.json({code: 200, info: `Inmueble ${id} Eliminado!`});
}

