import {Router} from 'express';

const router = Router();

import {deletePropietario, getIdPropietario, getPropietario, postPropietario, putPropietario} from '../Controllers/propietarios.Controllers';

router.route('/Propietario').get(getPropietario);
router.route('/Propietario/:id').get(getIdPropietario);
router.route('/Propietario').post(postPropietario);
router.route('/Propietario/:id').delete(deletePropietario);
router.route('/Propietario').put(putPropietario);

export default router;