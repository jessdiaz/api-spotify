import {Router} from 'express';

const router = Router();

import {deleteTipos, getIdTipos, getTipos, postTipos, putTipos} from '../Controllers/tipoInmueble.controllers';

router.route('/Tipo').get(getTipos);
router.route('/Tipo/:id').get(getIdTipos);
router.route('/Tipo').post(postTipos);
router.route('/Tipo/:id').delete(deleteTipos);
router.route('/Tipo').put(putTipos);

export default router;