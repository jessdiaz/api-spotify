import {Router} from 'express';

const router = Router();

import {getInmueble, getIdInmueble, postInmueble, putInmueble, deleteInmueble} from '../Controllers/Inmueble.Controllers';

router.route('/Inmueble').get(getInmueble);
router.route('/Inmueble/:id').get(getIdInmueble);
router.route('/Inmueble').post(postInmueble);
router.route('/Inmueble/:id').delete(deleteInmueble);
router.route('/Inmueble').put(putInmueble);

export default router;