import express, {Application, Request, Response} from 'express';
import morgan from 'morgan';
import compression from 'compression';
import cors from 'cors';

import routertipo from './routes/tipoInmueble.routes';
import routerprop from './routes/propietario.routes';
import routerinm from './routes/inmueble.routes';

export class Server{

    private app: Application;

    constructor(){
        this.app = express();
        this.settings();
        this.middlewares();
        this.routes();
    }
    
    settings(){
        this.app.set('port', process.env.PORT || 40020);
    }
    
    middlewares(){
        this.app.use(morgan('dev'));
        this.app.use(express.json());
        this.app.use(cors());
        this.app.use(compression());
    }

    routes(){
        this.app.use(routertipo);
        this.app.use(routerprop);
        this.app.use(routerinm);
    }

    async listen(){
        await this.app.listen(this.app.get('port'));
        console.log(`Esta corriendo por el puerto ${this.app.get('port')}`);
    }
}